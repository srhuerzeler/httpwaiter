"""Interface for Python3's http.Server."""

import http.server
import threading

import httpwaiter.http

class Interface (httpwaiter.http.Interface) :
    """HTTP Server to HTTP Serve Server interface."""

    def __init__(self, *args, **kwargs) :
        super().__init__(*args, **kwargs)

        self.http_server = http.server.HTTPServer(
            (self.waiter.address, self.waiter.port),
            _Handler)
        """Instance of python's HTTP server."""

        self.http_server_thread = threading.Thread(
            target = self.http_server.serve_forever)
        """Instance of http server thread."""

        # Reference to get back here.
        self.http_server.interface = self

        return
    
    def start(self) :
        self.http_server_thread.start()
        return
    def stop(self) :
        self.http_server.shutdown()
        self.http_server_thread.join()
        return

    pass


class _Handler (http.server.BaseHTTPRequestHandler) :
    """Handler for python HTTP server."""

    def __init__(self, *args, **kwargs) :
        super().__init__(*args, **kwargs)
        return

    def handle_one_request(self):
        """Override for custom request handling."""
        try:
            self.raw_requestline = self.rfile.readline(65537)
            if len(self.raw_requestline) > 65536:
                self.requestline = ''
                self.request_version = ''
                self.command = ''
                self.send_error(HTTPStatus.REQUEST_URI_TOO_LONG)
                return
            if not self.raw_requestline:
                self.close_connection = True
                return
            if not self.parse_request():
                return

            ###################################################################

            # Build request from handler data.
            request = httpwaiter.http.Request()
            request.method = self.command
            request.path = self.path
            request.version = self.request_version
            for header in self.headers :
                request.headers[header] = self.headers[header]
                pass

            # Read request body if content length was given.
            content_length = 0
            request.body = self.rfile.read(content_length)

            # Invoke on request to get response.
            response = self.server.interface.invoke_request(request)

            # Send response header.
            self.send_response(response.code)
            for header in response.headers :
                self.send_header(header, response.headers[header])
                pass
            self.end_headers()

            # Send response body.
            response_body = b''
            if response.body is not None :
                response_body = response.body
            self.wfile.write(response_body)
            self.wfile.flush()

            pass
        except TimeoutError as e:
            self.close_connection = True
            pass

        return

    def log_message(self, format, *args) :
        """Invoked by Python 3 HTTP server to log message."""
        return super().log_message(format, *args)

    pass

