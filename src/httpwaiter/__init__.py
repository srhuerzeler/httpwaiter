"""HTTP Serves Package."""

import httpwaiter.serve
import httpwaiter.http


class Waiter :
    """Main instance to serve http requests."""

    def __init__(self, address='0.0.0.0', port=8080, interface_cls=None) :
        """Initialize main server instance.

        Parameters:
            address:
                HTTP server address.
            port:
                HTTP server port.
            interface_cls:
                Class of HTTP server interface.
        """

        self.address = address
        """HTTP server address."""

        self.port = port
        """HTTP server port."""

        self.interface = None
        """Stores HTTP server interface."""

        self.serves = []
        """List containing possible targets."""

        # Initialize interface if interface class given.
        if interface_cls is not None :
            self.init_interface(interface_cls)
            pass

        return

    def __str__(self) :
        return self.__repr__()
    def __repr__(self) :
        return f'{type(self).__name__}(address={repr(self.address)}, port={repr(self.port)})'

    def init_interface(self, interface_cls) :
        """Initializes interface from interface class.

        Parameters:
            interface_cls:
                Class of interface to initialize.
        """
        self.interface = interface_cls(waiter=self)
        return

    def add_serve(self, serve_cls, *args, **kwargs) :
        """Add serve object to this server.
        Takes arguments to initialize serve.

        Parameters:
            serve_cls:
                Class of serve object to add.
        """
        serve = serve_cls(*args, **kwargs, waiter=self)
        self.serves.append(serve)
        return serve

    def match_serves(self, request) :
        """Matches request to find potential serves.

        Parameters:
            request:
                Request to find related serves.

        Returns:
            List of serves ordered by matching weight.
        """

        # List all serves returning weight.
        matching_serves = []
        for serve in self.serves :
            weight = serve.match(request)
            if weight is not None :
                matching_serves.append(serve)
                pass
            pass

        # Order matching serves.
        def get_weight(serve) :
            return serve.match(request)
        matching_serves.sort(key=get_weight, reverse=True)

        return matching_serves

    def serve(self, serves, request) :
        """Run serve or fallback on given request.

        Parameters:
            serves:
                List of serves, ordered by proprity, used for potential fallback.

        Returns:
            Response of serve.
            `None` if all serves failed.
        """
        for serve in serves :

            # Prepare response.
            response = httpwaiter.http.Response()
            request.response = response
            response.request = request

            # Serve and return if passed.
            passed = serve.serve(request, response)
            if passed :
                return response
            pass
        return None

    def handle_request(self, request) :
        """Handles given request.

        Parameters:
            request:
                Request to handle.
        """
        matching_serves = self.match_serves(request)
        response = self.serve(matching_serves, request)
        return response

    pass



