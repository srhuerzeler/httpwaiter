"""HTTP objects module."""


class Request :
    """Represents a HTTP request."""

    def __init__(self) :

        self.response = None
        """Reference to related response if existing."""

        self.method = 'NONE'
        """HTTP request method."""

        self.path = '/'
        """Requested path."""

        self.version = 'HTTP/1.1'
        """Request HTTP protocol version."""
        
        self.headers = {}
        """Dictionary of HTTP request headers."""

        self.body = None
        """Request body."""

        return

    def __str__(self) :
        return self.__repr__()
    def __repr__(self) :
        return f'Request({self.request_line_str})'

    @property
    def request_line_str(self) :
        """Compose string of request line."""
        return f'{self.method} {self.path} {self.version}'

    pass


class Response :
    """Represents a HTTP response."""
    def __init__(self) :

        self.request = None
        """Request of this response if eisting."""

        self.version = 'HTTP/1.1'
        """Response HTTP protocol version."""

        self.code = 200
        """Response status code."""

        self.msg = 'OK'
        """Response status message."""

        self.headers = {}
        """Dictionary of HTTP response headers."""

        self.body = None
        """Response body."""

        return

    def __str__(self) :
        return self.__repr__()
    def __repr__(self) :
        return f'Response({self.response_line_str})'

    @property
    def response_line_str(self) :
        """Compose string of response line."""
        return f'{self.version} {self.code} {self.msg}'

    pass


class Interface :
    """Interface superclass allowing HTTP Serve server to interact with HTTP servers."""

    def __init__(self, waiter) :
        """Initialize interface.

        Parameters:
            waiter:
                HTTP Serve server instance.
        """

        self.waiter = waiter
        """Reference to related server instance."""

        return

    def __str__(self) :
        return self.__repr__()
    def __repr__(self) :
        return f'Interface({self.server})'

    def start(self) :
        """Start HTTP server and return."""
        return
    def stop(self) :
        """Shutdown the HTTP server and return."""
        return

    def invoke_request(self, request) :
        """Handle incoming request.

        Parameters:
            request:
                Request to handle.

        Returns:
            Response to given request.
            `None` if request failed for unknown reason.
        """
        return self.waiter.handle_request(request)

    pass


