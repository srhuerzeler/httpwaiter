"""Serve file classes module."""

import pathlib
import re

import httpwaiter
import httpwaiter.http
import httpwaiter.serve


default_index_patterns = [
    r'index.html'
]
"""List of default index patterns."""

default_mime_patterns = [
    (r'.*\.js$',    'application/javascript'),
    (r'.*\.json$',  'application/json'),
    (r'.*\.pdf$',   'application/pdf'),
    (r'.*\.ogg$',   'audio/ogg'),
    (r'.*\.woff$',  'font/woff'),
    (r'.*\.woff2$', 'font/woff2'),
    (r'.*\.ttf$',   'font/ttf'),
    (r'.*\.jpg$',   'image/jpeg'),
    (r'.*\.png$',   'image/png'),
    (r'.*\.svg$',   'image/svg+xml'),
    (r'.*\.txt$',   'text/plain'),
    (r'.*\.css$',   'text/css'),
    (r'.*\.csv$',   'text/csv'),
    (r'.*\.html$',  'text/html'),
    (r'.*\.xml$',   'text/xml'),
    (r'.*',         'text/plain') # Fallback type.
]
"""List of default MIME type patterns."""

def find_index(path, index_patterns) :
    """Attempts to find index file in given directory path.

    Parameters:
        path:
            Path of directory to find index file in.
        index_patterns:
            List of patterns for index files matched to file name.
            Returns first match.

    Returns:
        Path of matching index file or path if no index file was found.
    """

    for index_pattern in index_patterns :
        for file in path.iterdir() :
            if not file.is_file() :
                continue
            if re.fullmatch(index_pattern, file.name) is not None :
                return file
            pass
        pass

    return path

def find_mime(path, mime_patterns) :
    """Attempts to find MIME type of a given file.

    Parameters:
        path:
            File to find MIME type of.
        nime_patterns:
            List of tuples containing [('pattern', 'type')].

    Returns:
        First file name matching MIME type of patterns list.
        `None` if no match was found.
    """

    for pattern, mime in mime_patterns :
        if re.fullmatch(pattern, path.name) :
            return mime
        pass

    return None


class REP_FileContents (httpwaiter.serve.REP_Serve) :
    """Byte content of a file."""

    def __init__(self, path, *args, mime_type=None, **kwargs) :
        """Serve file contents.

        Parameters:
            path:
                Path of file contents to serve,
            mime_type:
                MIME type of responded content.
                Tries to resolve from default patterns if `None`.
        """
        super().__init__(*args, **kwargs)

        self.path = pathlib.Path(path).resolve()
        """Serve file path."""

        self.mime_type = mime_type
        """MIME type of served file."""

        return

    def serve(self, request, response):

        # Get file binary contents.
        file_bytes = self.path.read_bytes()

        # Set response headers.
        response.headers['Content-Length'] = len(file_bytes)

        # Resolve MIME type.
        response.headers['Content-Type'] = self.mime_type
        if self.mime_type is None :
            response.headers['Content-Type'] = find_mime(self.path, default_mime_patterns)
            pass

        # Write file bytes to body.
        response.body = file_bytes

        return True

    pass


class REP_FileTree (httpwaiter.serve.REP_Serve) :
    """Serve file tree relative after given path."""

    def __init__(self, root_path, *args, **kwargs) :
        """Serve file tree.

        Parameters:
            root_path:
                Path of tree root directory to serve files from.
        """
        super().__init__(*args, **kwargs)

        self.root_path = pathlib.Path(root_path).resolve()
        """Root path of file tree to serve."""

        self.fallback_file = None
        """Path of file to serve as fallback."""

        self.index_patterns = default_index_patterns.copy()
        """Patterns to find index file."""

        self.mime_patterns = default_mime_patterns.copy()
        """Patterns to determine a file's MIME type."""

        return

    def serve(self, request, response) :

        # Get requested file to respond.
        rel_request_path = self.rep_match(request).groupdict()['req_rel']
        requested_path = self.root_path.joinpath(f'./{rel_request_path}').resolve()

        # Get index if requested path is a directory.
        # TODO: Foward to index instead of returning it. Fixes relative resources request.
        if requested_path.is_dir() :
            requested_path = find_index(requested_path, self.index_patterns)
            pass

        # Fallback if not existing.
        if not requested_path.exists() :
            response.code = 404
            return self.serve_fallback(request, response)

        # TODO: fallback blacklist.

        # Serve requested file.
        response.body = requested_path.read_bytes()
        response.headers['Content-Length'] = len(response.body)
        response.headers['Content-Type'] = find_mime(requested_path, self.mime_patterns)

        return True

    def serve_fallback(self, request, response) :
        """Invoked as fallback if requested file cannot be served."""

        if self.fallback_file is None:
            return False
        
        # Serve fallback file.
        response.body = self.fallback_file.read_bytes()
        response.headers['Content-Length'] = len(response.body)
        response.headers['Content-Type'] = find_mime(self.fallback_file, self.mime_patterns)

        return True

    pass



