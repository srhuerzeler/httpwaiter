"""Serve classes module."""


import pathlib
import re

import httpserves
import httpserves.http


class Serve :
    """Servable object superclass."""

    def __init__(self, waiter) :
        """Initialize serveable object.

        Parameters:
            server:
                Related HTTP Serve server instance.
        """

        self.waiter = waiter
        """Reference to related HTTP Serve server instance."""

        return

    def match(self, request) :
        """Check if given request could match this serve object.

        Parameters:
            request:
                HTTP request to check if potential match.

        Returns:
            Weight value of potential to be a match.
            If `None` is returned, this is considered as deffinetly not a match.
        """
        return None

    def serve(self, request, response):
        """Invoked to serve a request.
        
        Parameters:
            request:
                HTTP request instance of requesting this serve.
            response:
                After invoking, this is the response sent back.

        Returns:
            Boolean if this serve has completed.
            On `False` return, run the next potential serveable object by weight.
        """
        return False

    pass


class REP_Serve (Serve) :
    """Serveable object superclass, matching on a given regex pattern of HTTP request path."""

    def __init__(self, match_pattern, match_weight, *args, **kwargs) :
        """Initialize Regex path matching serve.

        Parameters:
            pattern:
                Regex pattern to check for matching request path.
            match_weight:
                Returned weight on matching pattern.
        """
        super().__init__(*args, **kwargs)

        self.match_pattern = match_pattern
        """Regex pattern to match with request path."""

        self.match_weight = match_weight
        """Returned weight on matching regex pattern."""

        return

    def rep_match(self, request) -> re.Match :
        """Match given request and returns regex pattern.

        Parameters:
            request:
                Request to get match object of.

        Returns:
            Match object or `None` if unable to get match.
        """
        return re.fullmatch(self.match_pattern, request.path)

    def match(self, request) :
        """Matches regex pattern to request path.

        Parameters:
            request:
                Request to match.

        Returns:
            Weight of this match or `None` if not matching.
        """

        # Match regex pattern to request path.
        re_match = self.rep_match(request)

        # Return match if matched.
        if re_match is not None :
            return self.match_weight
        return None

    pass


class REP_Bytes (REP_Serve) :
    """Responds with body of bytes."""

    def __init__(self, serve_bytes, *args, mime_type='text/plain', **kwargs) :
        """Serve bytes.

        Parameters:
            serve_bytes:
                Bytes to write to response body if matching.
            mime_type:
                MIME Type of responded bytes.
        """
        super().__init__(*args, **kwargs)

        self.serve_bytes = serve_bytes
        """Static bytes to be served."""

        self.mime_type = mime_type
        """MIME type of served bytes."""

        return

    def serve(self, request, response):

        # Set response headers.
        response.headers['Content-Length'] = len(self.serve_bytes)
        response.headers['Content-Type'] = self.mime_type

        # Set response body.
        response.body = self.serve_bytes

        return True

    pass


class REP_String (REP_Serve) :
    """Responds with body of stored string."""

    def __init__(self, serve_string, *args, mime_type='text/plain', **kwargs) :
        """Serve string.

        Parameters:
            serve_string:
                String to respond.
            mime_type:
                MIME type of reponse string.
        """
        super().__init__(*args, **kwargs)

        self.serve_string = serve_string
        """String to be served."""

        self.mime_type = mime_type
        """MIME type of served string."""

        return

    def serve(self, request, response):

        # Encode string.
        serve_bytes = self.serve_string.encode('utf-8')

        # Set response headers.
        response.headers['Content-Length'] = len(serve_bytes)
        response.headers['Content-Type'] = self.mime_type

        # Set response body.
        response.body = serve_bytes

        return True

    pass


class REP_HTTPRedirect (REP_Serve) :
    """Uses HTTP headers to redirect."""

    def __init__(self, location, *args, **kwargs) :
        """HTTP redirect.

        Parameters:
            location:
                URL to redirect to.
        """
        super().__init__(*args, **kwargs)

        self.location = location
        """Redirect location."""

        return

    def serve(self, request, response):

        # Set redirect response code.
        response.code = 307

        # Set response headers.
        response.headers['Location'] = self.location

        return True

    pass


