#!/usr/bin/env -S PYTHONPATH=./src python3 -B


import pathlib

import httpwaiter
import httpwaiter.serve
import httpwaiter.serve_files
import httpwaiter.py3http_interface


# Initialize server.
waiter = httpwaiter.Waiter(
    address = '0.0.0.0',
    port = 8080,
    interface_cls = httpwaiter.py3http_interface.Interface
)

waiter.add_serve(
    httpwaiter.serve.REP_String,
    'FALLBACK',
    '.*', 1)

waiter.add_serve(
    httpwaiter.serve.REP_Bytes,
    b'my static bytes',
    '/bytes', 100)

waiter.add_serve(
    httpwaiter.serve.REP_String,
    'My String to serve',
    '/string', 100)

waiter.add_serve(
    httpwaiter.serve.REP_HTTPRedirect,
    'http://example.com',
    '/redirect', 100)

waiter.add_serve(
    httpwaiter.serve_files.REP_FileContents,
    '/home/srh/httpwaiter/dev.py',
    '/file', 100)

serve_tree = waiter.add_serve(
    httpwaiter.serve_files.REP_FileTree,
    './builds/conditor.sphinx.Document-420/out',
    '(/tree)(?P<req_rel>[^?]*)(.*)', 100)
#serve_tree.fallback_file = pathlib.Path('./dist/conditor.sphinx.html-DEV/index.html')


waiter.interface.start()
input()
waiter.interface.stop()

