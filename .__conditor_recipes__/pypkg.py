"""Build recipes for the httpwaiter python package."""

import conditor.recipe
import conditor.py3pkg.recipe
import conditor.py3pkg.build

DEFAULT_RECIPE = 'HTTPWaiter'


class HTTPWaiter (conditor.py3pkg.recipe.Package) :
    """Default httpwaiter python package build."""
    NAME = 'httpwaiter'
    def configure(self, build, flavour) :
        package_root = self.project.path.joinpath('./src/httpwaiter').resolve()
        package = conditor.py3pkg.build.Package(build)
        build['path.package_source'] = str(package_root)
        build['py3pkg.explicit_packages'] = ['httpwaiter', 'conditor.compose']
        self.configure_clone_locals(build, self.project.L['./src/httpwaiter'])
        return
    pass


