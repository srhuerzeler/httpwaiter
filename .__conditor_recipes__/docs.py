
import conditor.recipe

DEFAULT_RECIPE = 'HTML'

class HTML (conditor.recipe.Recipe) :
    STAGES = ['init', 'configure', 'deploy', 'depends', 'compose', 'build']
    NAME = 'docs.HTML'
    def get_bake(self, build) :
        return build.project.build[build['build.bake.name']]
    def get_sphinx(self, build) :
        return build.project.build[build['build.sphinx.name']]
    def init(self, build, flavour) :
        self.entry.recipe_manager.import_recipe('conditor.docs_baker.recipe')
        self.entry.recipe_manager.import_recipe('conditor.sphinx.recipe')
        bake_build = self.project.recipe['conditor.docs_baker.recipe'].default.new_build('420')
        build['build.bake.name'] = bake_build.name
        sphinx_build = self.project.recipe['conditor.sphinx.recipe'].default.new_build('420')
        build['build.sphinx.name'] = sphinx_build.name
        # Document data
        build['docs.project'] = self.project.G['project.title']
        build['docs.author'] = 'Custom Author'
        build['docs.copyright'] = 'cpr'
        return
    def configure(self, build, flavour) :
        bake_build = self.get_bake(build)
        sphinx_build = self.get_sphinx(build)
        bake_build['doc_baker.doc_name'] = 'conditor'
        sphinx_build['path.include_pythonpath_roots'] = [str(self.project.path.joinpath('./src').resolve())]
        #bake_build[]
        #sphinx_build[]
        return
    def deploy(self, build, flavour) :
        return
    def depends(self, build, flavour) :
        bake_build = self.get_bake(build)
        sphinx_build = self.get_sphinx(build)
        bake_build.stage_all()
        sphinx_build['path.conf_origin'] = str(self.project.path.joinpath('./docs/conf.py').resolve())
        sphinx_build['path.source_origin'] = bake_build['path.out.sphinx']
        sphinx_build['__propextend_build__'] = build.name
        sphinx_build.stage_all()
        return
    def compose(self, build, flavour) :
        return
    def build(self, build, flavour) :
        #sphinx_build = self.project.recipe['conditor.sphinx.recipe'].new_build('420')
        return
    pass

